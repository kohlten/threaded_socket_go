package threaded_socket

// #cgo pkg-config: threaded_socket glib-2.0
// #include "number.h"
// #include "threaded_socket_time.h"
import "C"

func GetCurrentTime() uint64 {
  return uint64(C.get_current_time())
}

func Delay(ms uint64) {
  C.delay(C.u64(ms))
}
