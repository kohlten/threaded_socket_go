package threaded_socket

// #cgo pkg-config: threaded_socket glib-2.0
// #include <stdlib.h>
// #include "number.h"
// #include "socket.h"
// #include "threaded_socket_data.h"
// #include "threaded_socket_init.h"
import "C"
import "unsafe"

func InitTSocketClientFromServer(server *Socket) (*TSocket, TSocketStatus) {
    var status TSocketStatus
    client := C.init_tsocket_client_from_server((*C.struct_s_socket)(unsafe.Pointer(server)), (*C.t_tsocket_status)(&status))
    return client, uint32(status)
}

func InitTSocketClient(address string, port uint16) (*TSocket, TSocketStatus) {
    var status TSocketStatus
    cAddress := C.CString(address)
    defer C.free(unsafe.Pointer(cAddress))
    client := C.init_tsocket_client((*C.s8)(cAddress), C.u16(port), (*C.t_tsocket_status)(&status))
    return client, uint32(status)
}
